insert into public.fighters (id, name, health, attack, defense, source) values
(1, 'Ryu', 45, 4, 3, 'http://www.fightersgeneration.com/np5/char/ssf2hd/ryu-hdstance.gif'),
(2, 'Dhalsim', 60, 3, 1, 'http://www.fightersgeneration.com/np5/char/ssf2hd/dhalsim-hdstance.gif'),
(3, 'Guile', 45, 4, 3, 'http://www.fightersgeneration.com/np5/char/ssf2hd/guile-hdstance.gif'),
(4, 'Zangief', 60, 4, 1, 'http://www.fightersgeneration.com/np5/char/ssf2hd/zangief-hdstance.gif'),
(5, 'Ken', 45, 3, 4, 'http://www.fightersgeneration.com/np5/char/ssf2hd/ken-hdstance.gif'),
(6, 'Bison', 45, 5, 4, 'http://www.fightersgeneration.com/np5/char/ssf2hd/bison-hdstance.gif'),
(7, 'Chun-Li', 40, 3, 8, 'http://www.fightersgeneration.com/np5/char/ssf2hd/chunli-hdstance.gif'),
(8, 'Blanka', 80, 8, 2, 'http://www.fightersgeneration.com/np5/char/ssf2hd/blanka-hdstance.gif'),
(9, 'E.Honda', 50, 5, 5, 'http://www.fightersgeneration.com/np5/char/ssf2hd/ehonda-hdstance.gif'),
(10, 'Balrog', 55, 4, 6, 'http://www.fightersgeneration.com/np5/char/ssf2hd/balrog-hdstance.gif'),
(11, 'Vega', 50, 4, 7, 'http://www.fightersgeneration.com/np5/char/ssf2hd/vega-hdstance.gif'),
(12, 'Sagat', 55, 4, 6, 'http://www.fightersgeneration.com/np5/char/ssf2hd/sagat-hdstance.gif'),
(13, 'Cammy', 45, 4, 7, 'http://www.fightersgeneration.com/np5/char/ssf2hd/cammy-hdstance.gif'),
(14, 'Fei Long', 80, 2, 3, 'http://www.fightersgeneration.com/np5/char/ssf2hd/feilong-hdstance.gif'),
(15, 'Dee Jay', 50, 5, 5, 'http://www.fightersgeneration.com/np5/char/ssf2hd/deejay-hdstance.gif'),
(16, 'T.Hawk', 60, 5, 3, 'http://www.fightersgeneration.com/np5/char/ssf2hd/thawk-hdstance.gif'),
(17, 'Akuma', 70, 5, 5, 'http://www.fightersgeneration.com/np5/char/ssf2hd/akuma-hdstance.gif');

SELECT setval('fighters_id_seq', 17);