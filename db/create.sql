drop table if exists public.fighters;

create table public.fighters (
  id        serial      primary key,
  name      varchar(50) NOT NULL,
  health    integer     NOT NULL,
  attack    integer     NOT NULL,
  defense   integer     NOT NULL,
  source    varchar     NOT NULL
);
