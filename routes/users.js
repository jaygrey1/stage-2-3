const express = require('express');

const userService = require('../services/user.service');

const router = express.Router();


router.get('/', async function (request, response) {
  response.status(200).json(await userService.getAll());
});

router.get('/:id(\\d+)', async function (request, response) {
  const user = await userService.get(request.params.id);
  if (user != null) {
    response.status(200).json(user);
  } else {
    response.status(404).end();
  }

});

router.post('/', async function (request, response) {
  const user = await userService.save(request.body);
  if (user != null) {
    response.status(201).json(user);
  } else {
    response.status(400).end();
  }
});

router.put('/:id(\\d+)', async function (request, response) {

  if (await userService.update(request.params.id, request.body)) {
    response.status(204).end();
  } else {
    response.status(400).end();
  }
});

router.delete('/:id(\\d+)', async function (request, response) {
  if (await userService.delete(request.params.id)) {
    response.status(204).end();
  } else {
    response.status(404).end();
  }
});

module.exports = router;
