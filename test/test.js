const assert = require('assert');
const userRepository = require('../repositories/user.repository');

describe('UserRepository', function () {
    describe('#get()', function () {

        beforeEach(function () {
            userRepository._users.clear();
        });

        it('should return null on empty repository', function () {
            assert.equal(userRepository.get(1), null);
            assert.equal(userRepository._users.size, 0);
        });

        it('should return null on invalid keys', function () {
            assert.equal(userRepository.get('abc'), null);
        });

        it('should return null on null keys', function () {
            assert.equal(userRepository.get(null), null);
        });

        it('should return same object that has been saved before', function () {
            let object = { a: 1, b: 2, c: "3" };

            object = userRepository.save(object);
            assert.deepStrictEqual(userRepository.get(object.id), object);
        })
    });

    describe('#getAll()', function () {
        beforeEach(function () {
            userRepository._users.clear();
        });

        it('should return empty array on empty repository', function () {
            assert.equal(userRepository.getAll().length, 0);
        });
        it('should return non empty array on filled repository', function () {
            userRepository.save({ a: 1, b: 2 });
            assert.equal(userRepository.getAll().length, 1);
        });
    });

    describe('#save()', function () {
        beforeEach(function () {
            userRepository._users.clear();
        });

        it('should return null when save null object', function () {
            assert.equal(userRepository.save(null), null);
        });

        it('should return null when object already has id key', function () {
            assert.equal(userRepository.save({ id: 1, a: 2, b: 3 }), null);
        });

        it('should add id key to object', function () {
            let object = { a: 1, b: 2, c: 3 };
            userRepository.save(object);

            assert.equal('id' in object, true);
        });

        it('two sequential saved object must have different id', function () {

            const object1 = userRepository.save({ a: 1, b: 2, c: 3 });
            const object2 = userRepository.save({ a: 1, b: 2, c: 3 });

            assert.equal(object1.id != object2.id, true);
        });
    });

    describe('#update()', function () {
        beforeEach(function () {
            userRepository._users.clear();
        });

        it("should return false when object doesn't have id key", function () {
            const object = userRepository.update({ a: 1, b: 2, c: 3 });
            assert.strictEqual(object, false);
        });

        it("should return true and contains same object on sucessfull update", function () {
            const object1 = userRepository.save({ a: 1, b: 2, c: 3 });
            object1.a = 10;
            assert.equal(userRepository.update(object1), true);

            const object2 = userRepository.get(object1.id);
            assert.strictEqual(object1, object2);
        });
    });

    describe('#delete()', function () {
        beforeEach(function () {
            userRepository._users.clear();
        });

        it("should return false on delete nonexist object", function () {
            assert.equal(userRepository.delete(10), false);
            assert.equal(userRepository.delete(null), false);
            assert.equal(userRepository.delete('abc'), false);
            assert.equal(userRepository.delete(), false);
        });

        it("should return true on delete existent object", function () {
            const object = userRepository.save({a:1, b:2});
            assert.equal(userRepository.delete(object.id), true);
            assert.equal(userRepository.delete(object.id), false);
        });
    });
});
