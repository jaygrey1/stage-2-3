//const userRepository = require('../repositories/user.repository.fs');

const userRepository = require('../repositories/fighter.repository.db');

class UserService {
    constructor() {
        this._repository = userRepository;
    }

    _convertToInteger(id) {
        if (typeof id === 'string') {
            return parseInt(id, 10);
        }

        return id;
    }

    async get(id) {
        return await this._repository.get(this._convertToInteger(id));
    }

    async getAll() {
        return await this._repository.getAll();
    }

    async save(user) {
        return await this._repository.save(user);
    }

    async update(id, user) {
        user.id = this._convertToInteger(id);
        return await this._repository.update(user);
    }

    async delete(id) {
        return await this._repository.delete(this._convertToInteger(id));
    }
}

const userService = new UserService();

module.exports = userService;