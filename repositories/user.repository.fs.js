const fs = require('fs');
const fsPromises = require('fs').promises;


class UserRepository {
    constructor() {
        this._filepath = './repositories/users.json';
        this._users = new Map();
        this._idGenerator = 0;
        
        (async () => await this._loadList())();
    }


    async _loadList() {

        let fHandle = null;
        try {
            fHandle = await fsPromises.open(this._filepath, 'r');
            let data = await fHandle.readFile({encoding: 'utf8', flag: 'r'});
            data = JSON.parse(data);
            data.forEach(element => {
                this._users.set(element.id, element);
                this._idGenerator = Math.max(this._idGenerator, element.id);
            });
            this._idGenerator++;
        } catch (error) {
            console.log(error);
        } finally {
            if (fHandle != null) {
                await fHandle.close();
            }
        }
    }

    async _saveList() {
        let fHandle = null;
        try {
            fHandle = await fsPromises.open(this._filepath, 'w');
            await fHandle.writeFile(JSON.stringify([...this._users.values()]), {encoding: 'utf8', flag: 'w'});
            await fHandle.datasync();
        } catch(error) {
            console.log(error);
        } finally {
            if (fHandle != null) {
                await fHandle.close();
            }
        }
    }

    get(id) {
        if (this._users.has(id)) {
            return this._users.get(id);
        }

        return null;
    }

    getAll() {
        return [...this._users.values()];
    }

    async save(user) {
        if (user != null && !('id' in user)) {
            user.id = this._idGenerator++;
            this._users.set(user.id, user);
            this._saveList();
            return user;
        }

        return null;
    }

    async update(user) {
        if (user != null && 'id' in user && this._users.has(user.id)) {
            this._users.set(user.id, user);
            this._saveList();
            return true;
        }

        return false;
    }

    async delete(id) {
        if (this._users.has(id)) {
            this._users.delete(id);
            this._saveList();
            return true;
        }

        return false;
    }
}

const userRepository = new UserRepository();

module.exports = userRepository;