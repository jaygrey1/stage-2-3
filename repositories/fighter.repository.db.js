﻿const pgp = require('pg-promise')();


class FighterRepository {
    constructor() {
        this._tableName = 'public.fighters';
        this._fields = new Set(['name', 'health', 'attack', 'defense', 'source']);
        this._db = pgp(process.env.DATABASE_URL);
    }

    _isDefined(object) {
        return object != null && object != undefined;
    }

    async getAll() {
        const queryString = 'select id, name, health, attack, defense, source from public.fighters';

        try {
            return await this._db.any(queryString);
        }
        catch(error) {
            console.error(error);
            return null;
        }
    }

    async get(id) {

        if(! Number.isInteger(id)) {
            return null;
        }

        const queryString = 'SELECT id, name, health, attack, defense, source from public.fighters where id = $<id>';
        const values = {id: id};

        try {
            return await this._db.one(queryString, values);
        }
        catch(error) {
            console.error(error);
            return null;
        }
    }

    async save(fighter) {
        if (this._isDefined(fighter) && !('id' in fighter)) {

            const queryString = 'insert into public.fighters(name, health, attack, defense, source) values ($<name>, $<health>, $<attack>, $<defense>, $<source>) returning id;';
            const values = {
                name: fighter.name || '',
                health: fighter.health || 1,
                attack: fighter.attack || 1,
                defense: fighter.defense || 1,
                source: fighter.source || ''
            };

            try {
                fighter.id = (await this._db.one(queryString, values)).id;

                return fighter;
            } catch(error) {
                console.error(error);
                return null;
            }
        }

        return null;
    }


    _buildUpdateQuery(object) {
        if (! this._isDefined(object)) {
            return '';
        }

        let queryString = `update ${this._tableName} set `;
        let values = {id: object.id};

        let fieldSeparator = '';

        for (let key in object) {
            if (this._fields.has(key)) {
                queryString = queryString.concat(`${fieldSeparator}${key}=$<${key}>`);
                values[key] = object[key];
                fieldSeparator = ', ';
            }
        }
        queryString = queryString = queryString.concat(' where id = $<id>');

        return {text: queryString, params: values};
    }

    async update(fighter) {
        if (this._isDefined(fighter) && 'id' in fighter && Number.isInteger(fighter.id)) {

            const query = this._buildUpdateQuery(fighter);

            try {
                await this._db.none(query.text, query.params);
                return true;
            } catch (error) {
                console.error(error);
                return false;
            }

        }
        return false;
    }

    async delete(id) {
        if (Number.isInteger(id)) {
            const queryString = 'delete from public.fighters where id = $<id>';
            const values = {id: id};
            try {
                await this._db.none(queryString, values);
                return true;
            } catch (error) {
                console.error(error);
                return false;
            }
        }

        return false;
    }
}

const fighterRepository = new FighterRepository();

module.exports = fighterRepository;
