const express = require('express');
const logger = require('morgan');
const cors = require('cors');

const usersRouter = require('./routes/users');

const app = express();

app.use(logger('combined'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));


const corsOptions = {methods: ['GET', 'PUT', 'POST', 'DELETE']};

app.use('/user', cors(corsOptions), usersRouter);

app.use((request, response, next) => {
    response.status(400).end();    
});


module.exports = app;
